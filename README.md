** BRA API ** 

Swagger URL for the operations https://falco-prod-api-ws-falco-stage-api-ws.azurewebsites.net/swagger/ui/index#!/Bank/
## Generate a client that should use this API Controller and implement calls for
  - https://falco-prod-api-ws-falco-stage-api-ws.azurewebsites.net/api/bank/CreateTraveler
  - https://falco-prod-api-ws-falco-stage-api-ws.azurewebsites.net/api/bank/GetMember/{id}
  - https://falco-prod-api-ws-falco-stage-api-ws.azurewebsites.net/api/bank/UpdateTraveler
  - https://falco-prod-api-ws-falco-stage-api-ws.azurewebsites.net/api/bank/DupeCheck
  
## Append headers to calls
  - content-type : application/json
  - falcocustomheader : 
  {
  "Id":1,
  "CultureInfoName":"sv",
  "IsAuthenticated":true,
  "FrequentFlyerId":"Set to any string but should be set to frequentflyerid of member when calling update",
  "WebSessionId":Guid.NewGuid().ToString(),
  "Hash":"a2c556bf4f99e4d64a9049b7a57892d2942f2174d61b482d8832eebe0e8f7d96" 
  }
##  Make a request to CreateTraveler , sample json data. Before you make the call perform a check call to DupeCheck method so that you ensure the user dont exist
{
  "Username": "sssSSSEEEEeeee",
  "Password": "Jagtest1984",
  "Gender": 0,
  "Firstname": "Testar",
  "Surname": "Akerberg",
  "CountryCodePhone": "46",
  "Phone": "70456487",
  "Email": "sssSSSEEEEeeee@asdsda.se",
  "OrganizationFrequentFlyerId": null,
  "DateOfBirth": "1984-06-08T00:00:00",
  "EnrollmentSource": "TF",
  "NewsAndOffers": true,
  "BankInformation": {
    "AccountNumber": "12313133",
    "StartDate": "2018-01-01T00:00:00",
    "EndDate": "2019-01-01T00:00:00",
    "PartnerName": "BRABank",
    "Active": true
  }
}
## Make a request to GetMember/{id} where ID is the newly created member FrequentFlyerId and perform a call to UpdateTraveler, sample put json data
{
	"Firstname" : "Testar",
	"SurName" : "Akerberg",
	"Status" : "AC",
	"TravelerData": {
	"FrequentFlyerID" : "300000985",
	"BankInformation": {
      "AccountNumber": "12313133",
      "StartDate": "2018-01-01T00:00:00",
      "EndDate": "2019-01-01T00:00:00",
      "PartnerName": "BRAbank",
      "Active": true
	}
   }
}
Make sure you change the FrequentFlyerID in the Header before you make this call
